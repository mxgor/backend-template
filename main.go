package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging/common"
	pluginSdk "gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugins/core"
	"net/http"
)

func handlerApp(c *gin.Context) {
	c.JSON(http.StatusOK, `{"userId":1,"id":1,"title":"delectus aut autem","completed":false}`)
}

func handlerEvent(c *gin.Context) {
	topic := c.Param("topic")
	user := c.Request.Context().Value(pluginSdk.UserKey).(*pluginSdk.UserInfo)
	record := messaging.HistoryRecord{
		Reply: common.Reply{
			TenantId:  "tenant_space",
			RequestId: "123654",
			Error:     nil,
		},
		UserId:  user.ID(),
		Message: "Remarkable history event",
	}
	err := pluginSdk.PublishHistoryEvent(topic, messaging.PresentationRequest, record)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	} else {
		c.JSON(http.StatusOK, record)
	}
}

func main() {
	r := gin.Default()
	r.GET("/app", pluginSdk.AuthMiddleware(), handlerApp)

	metadata := pluginSdk.Metadata{
		Name:        "plugin-template",
		ID:          "2392323923",
		Description: "Template of the plugin",
	}
	r.GET("/metadata", pluginSdk.MetadataHandler(metadata))
	r.GET("/event/:topic", pluginSdk.AuthMiddleware(), handlerEvent)
	r.Run()
}
